var Promise = require('promise');
var common = require('./common.js');
const fs = require('fs');
const path = require('path');
const tinify = require("tinify");
tinify.key = "9MqclqXzOqCQB2p9gT7SmlO4i7zu62vQ";
var archiver = require('archiver');
const exiftool = require('node-exiftool');
const exiftoolBin = require('dist-exiftool');
var ProgressBar = require('progress');
var parser = require('xml2json');
const tmpDirectory = 'tmp/';
const reportDirectory = 'HTML_Report';
var gHTML = require('generate-html');
/**
Just return the size for a given file - Synchronous
*/
/*var giveFileSize = function(path) {
  const stats = fs.statSync(path);
  const fileSizeInBytes = stats.size;
  return fileSizeInBytes;
}*/

function writeMetadata( object ){
  return new Promise(function (fulfill, reject){
    var ep = new exiftool.ExiftoolProcess(exiftoolBin);
    ep
      .open()
      .then(() => ep.writeMetadata(object.imagePath, object.metadata, ['codedcharacterset=utf8','overwrite_original']))
      .then(() => ep.close())
      .done(fulfill(object.metadata))
      .catch((err) => {reject(err)});
  });
}

var optimizeImage =  function(imagePath, bar, index) {
  return new Promise(function (fulfill, reject){

    //pull metadata
    common.pullMetadata(imagePath)
      //then optimize
      .then((res) => {
        const sizeBefore = common.giveFileSizeSync(res.imagePath);
        const source = tinify.fromFile(res.imagePath);
        source.toFile(res.imagePath, function(err){
          if (err) {
            //console.log('error, ' + imagePath + ', ' + sizeBefore + ', N/A, N/A, err message: ', err.message);
            fulfill({success:false,sizeBefore, sizeAfter:0, path:res.imagePath.replace('tmp',''), activated:'N/A', modifiedAfterUpdate:'N/A', metadata:undefined, errMsg:err.message});
          } else {

            const sizeAfter = common.giveFileSizeSync(res.imagePath);

            var jcrContentPath = common.buildJcrContentNodePath(imagePath);
            common.readFile(jcrContentPath)
              .then((xml) => {
                var json = JSON.parse(parser.toJson(xml, {reversible: true}));
                //check is activated
                var activated = json['jcr:root']['jcr:content']['cq:lastReplicationAction'] == 'Activate';
                var lastReplicated = 'N/A';
                var lastModified = 'N/A';
                var modifiedAfterUpdate = 'N/A';
                if(json['jcr:root']['jcr:content']['cq:lastReplicated']){
                    lastReplicated = new Date(json['jcr:root']['jcr:content']['cq:lastReplicated'].replace('{Date}',''));
                    lastModified = new Date(json['jcr:root']['jcr:content']['jcr:lastModified'].replace('{Date}',''));
                    modifiedAfterUpdate = lastModified > lastReplicated;
                }

                //push the metadata now....
                var newMetadata = {};
                newMetadata['Comment'] = 'Optimized: ' + new Date().toISOString();
                var oldMetadata = res.metadata.data[0];
                if(oldMetadata.Title) newMetadata.Title = oldMetadata.Title;
                if(oldMetadata.Description) newMetadata.Description = oldMetadata.Description;

                if ( Object.keys(newMetadata).length > 0 ) {
                  writeMetadata({imagePath, metadata:newMetadata})
                    .done(function(res){
                        metadataWritten = Object.keys(res).toString();
                        bar.tick()

                        var imageFileName = common.getAssetFilename(imagePath);
                        fs.copyFile(imagePath, reportDirectory + '/resources/' + imageFileName + '_optimized', (err)=>{
                          if(err) throw err;
                        })


                        fulfill({success:true, sizeBefore, sizeAfter, path:imagePath, activated, modifiedAfterUpdate, metadata:metadataWritten, errMsg:undefined});
                    })
                } else {
                  fulfill({sucess:true, sizeBefore, sizeAfter, path:imagePath, activated, modifiedAfterUpdate, metadata:undefined, errMsg:undefined});
                }


              })
              .catch((err) => console.log(err));


          }
        });
      }).catch((err) => { console.log(err) });
  });
}

var processImages = function(images){
  return new Promise(function(fulfill, reject){

    //create html report folder
    fs.mkdir(reportDirectory + path.sep + 'resources', {recursive:true}, (err) => {
      if(err) throw err;

      images.forEach((image, index)=>{
        var imageFileName = common.getAssetFilename(image.path);
        fs.copyFile(image.path, reportDirectory + '/resources/' + imageFileName + '_original',  (err) => {
          if(err) console.log(err)
        })
      })

    });

    var actions = new Array();
    console.log()
    var bar = new ProgressBar('Processing :current of :total assets => [:percent Elapsed :elapseds] :bar', { total: images.length, width: 200 });
    images.forEach((image, index) => { actions.push(optimizeImage(image.path, bar, index)) })
    var results = Promise.all(actions);
    results.then(data => {
      fulfill(data);
      }
    ).catch(function(err){
      console.log('Some promises were executed with errors');
      console.log(err);
      reject(err);
    });

  });
}

var updatePropertiesFile = function(filename, packageName){
  return new Promise(function( fulfill, reject ){
    //update the package name
    common.readFile(filename)
      .then((data) => {
        var json = JSON.parse(parser.toJson(data, {reversible: true}));
        json.properties.entry.forEach(function(item){
          if(item.key=='name'){
            item.$t = packageName;
          }
          if(item.key=='lastModified'){
            item.$t = new Date();
          }
          if(item.key=='version'){
            var d = new Date();
            item.$t = d.getUTCFullYear() + '.' + (d.getUTCMonth() + 1) + '.' + d.getUTCDate();
          }
        });

        var header = '<?xml version="1.0" encoding="utf-8" standalone="no"?><!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">';
        var body = JSON.stringify(json);
        var xml = parser.toXml(body);
        xml = header.concat(xml);

        common.writeFile(tmpDirectory + 'META-INF/vault/properties.xml', xml)
          .then((res)=>{
            console.log(filename.replace('tmp','') + ' Update Success')
            fulfill()
          })
          .catch((err) => { reject(err) });
      } )
      .catch((err) => { reject(err) });

});
}

var updateFilterFile = function(filename){
  return new Promise(function(fulfill, reject){

    common.readFile(filename)
      .then((fileContent) => {
        var json = JSON.parse(parser.toJson(fileContent, {reversible: true}));
        //filter shouls only have /content/dam/gene/whateverSite
        json.workspaceFilter.filter = json.workspaceFilter.filter.filter(function(value, index, arr){
          return value.root.includes('/content/dam/gene');
        });

        json.workspaceFilter.filter.forEach((item) => { item.mode='update' });

        var header = '<?xml version="1.0" encoding="UTF-8"?>';
        var body = JSON.stringify(json);
        var xml = parser.toXml(body);
        xml = header.concat(xml);

        common.writeFile(filename, xml)
          .then((res)=>{
            console.log(filename.replace('tmp','') + ' Update Success')
            fulfill()
          })
          .catch((res) => { reject(err) });
      })
      .catch((err) => { reject(err) })
  });
}

var updateDefinitionFile = function(filename, packageName){
  return new Promise(function(fulfill, reject){
    common.readFile(filename)
      .then((fileContent)=>{
        var d = new Date();
        var json = JSON.parse(parser.toJson(fileContent, {reversible: true}));

        json['jcr:root']['jcr:lastModified']='{Date}'+d.toISOString();
        json['jcr:root']['version']= d.getUTCFullYear() + '.' + (d.getUTCMonth() + 1) + '.' + d.getUTCDate();
        json['jcr:root']['name']=packageName;

        //I just need the filter definition for /content/dam/gene
        var patt2 = /(f\d)/;
        var f0 = {};
        for (var key in json['jcr:root']['filter']) {
          if(key.match(patt2) && json['jcr:root']['filter'][key].root.includes('/content/dam/gene/')){
            f0=json['jcr:root']['filter'][key];
          }
        }

        //remove all other filter definitions
        for (var key in json['jcr:root']['filter']) {
          if(key.match(patt2)){
            delete json['jcr:root']['filter'][key]
          }
        }

        f0.mode='update';

        json['jcr:root']['filter']['f0']=f0;

        var header = '<?xml version="1.0" encoding="UTF-8"?>';
        var body = JSON.stringify(json);
        var xml = parser.toXml(body);
        xml = header.concat(xml);

        common.writeFile(filename, xml)
          .then((res)=>{
            console.log(filename.replace('tmp','') + ' Update Success')
            fulfill()
          })
          .catch((err) => { reject(err) });

      })
      .catch((err)=>{reject(err)});
  });
}

var generateReport = function(){
  return new Promise(function(resolve, reject){
    common.readDirectory(reportDirectory)
    .then((files)=>{

      fs.copyFile('resources/jquery.event.move.js', reportDirectory + '/resources/jquery.event.move.js', (err)=>{ if(err) console.log(err) })
      fs.copyFile('resources/jquery.twentytwenty.js', reportDirectory + '/resources/jquery.twentytwenty.js', (err)=>{if(err) console.log(err)})
      fs.copyFile('resources/foundation.css', reportDirectory + '/resources/foundation.css', (err)=>{if(err) console.log(err)})
      fs.copyFile('resources/twentytwenty-no-compass.css', reportDirectory + '/resources/twentytwenty-no-compass.css', (err)=>{if(err) console.log(err)})
      fs.copyFile('resources/twentytwenty.css', reportDirectory +'/resources/twentytwenty.css', (err)=>{if(err) console.log(err)})

      var html = '<!doctype html><html><head><link rel="stylesheet" type="text/css" href="resources/foundation.css"><link rel="stylesheet" type="text/css" href="resources/twentytwenty-no-compass.css"><link rel="stylesheet" type="text/css" href="resources/twentytwenty.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><script src="resources/jquery.event.move.js"></script>'+
      '<script src="resources/jquery.twentytwenty.js"></script>'+
'</head><body>';
      files.forEach((file,index)=>{
        if(file.includes('_original')){

          var tr = '<p>'+ index +' - '+ file +'</p>';
          tr=tr.concat('<div id="container'+index.toString().trim()+'" class="twentytwenty-container">')
          tr = tr.concat('<img src="'+file.replace(reportDirectory+path.sep, '')+'"><img src="'+file.replace(reportDirectory+path.sep, '').replace('original','optimized')+'">');
          tr=tr.concat('</div>');
          tr=tr.concat('<hr>')
          html=html.concat(tr);
        }
      })

      html=html.concat('<script>$( document ).ready(function() { $(".twentytwenty-container").twentytwenty(); });</script>');

      common.writeFile(reportDirectory+path.sep+'report.html', html)
        .then((res)=>{
          console.log(filename.replace('tmp','') + ' Update Success')
          resolve('ok report')
        })
        .catch((err) => { console.log(err); reject(err) });

    })
    .catch((err)=>{console.log(err); reject(err)});
  });
}

var zip = function(images, sourceZipFile, packageName){
  return new Promise(function(fulfill, reject){

    //package only the optimized assets
    var paths = new Array();
    var patt1=/(.jpg|.png)/;
    images.forEach(function(image){
      //m = image.match(patt1);
      //paths.push( image.substring(0, (m.index+4)) );
      m = image.path.match(patt1);
      paths.push( image.path.substring(0, (m.index+4)) );
    });

    var location = common.getLocationOfSourcePackage(sourceZipFile);
    filename = packageName;

    var output = fs.createWriteStream(location + filename + '.zip');
    var archive = archiver('zip',{
        zlib: { level: 9 } // Sets the compression level.
    });

    Promise.all([
      updatePropertiesFile(tmpDirectory + 'META-INF/vault/properties.xml', filename),
      updateFilterFile(tmpDirectory + 'META-INF/vault/filter.xml'),
      updateDefinitionFile(tmpDirectory + 'META-INF/vault/definition/.content.xml', filename)])
    .then(function(){
          console.log()
          console.log('zipping content to ' + location + filename + '.zip')
          //zip only the optimized images
          paths.forEach(function(path){
            archive.directory(path, path.replace('tmp',''));
          });

          //add META-INF package
          archive.directory(tmpDirectory+'META-INF', 'META-INF');

          output.on('close', function() {
            console.log(archive.pointer() + ' total bytes');
            fulfill('ok');
          });

          output.on('end', function() {
            console.log('Data has been drained');
          });

          archive.on('warning', function(err) {
            if (err.code === 'ENOENT') {
              console.log(err);
            } else {
              reject(err);
            }
          });

          archive.pipe(output);

          archive.finalize();

          archive.on('error', function(err) {
            reject(err);
          });


    })
    .catch((err) => { reject(err) });
  });
}

try {

  //validate arguments
  common.validateArgumentsForImageOptimization(process.argv)
  .then((args) => {
    //clear temp directory
    common.clearTmpDirectory(tmpDirectory)
    .then((directory) => {
      //unzip source content package on clear temp directory
      common.unzip(args.sourceZipFile, directory)
      .then((directory) => {
        //read directory and retrieve all files there
        common.readDirectory(directory)
        .then((images) => {
          //filter assets
          common.filterAssets(images, args.size)
            .then((filteredImages) => {
              //process filtered images
              processImages(filteredImages)
                .then((optimizedImages) => {
                  //zip optimized images
                  zip(optimizedImages, args.sourceZipFile, args.targetPackageName)
                    .then((res) => {
                      console.log()
                      console.log('Results...')
                      console.table(optimizedImages)
                      console.log()
                      console.log('Everything completed')
                      console.log()
                      //generate report
                      generateReport(optimizedImages)
                        .then((result) => console.log('Image comparison report was generated at HTML_Report/report.html'))
                        .catch((err) => console.log(err))
                    })
                    .catch((err) => console.log(err))
                })
                .catch((err) => console.log(err))
            })
            .catch((err) => console.log(err))
        })
        .catch((err) => console.log(err))
      })
      .catch((err) => console.log(err))
    })
    .catch((err) => console.log(err))
  })
  .catch((err)=>console.log(err))

} catch (e){
  console.log('Catched exception ')
  console.log(e)
}

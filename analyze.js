var Promise = require('promise');
var common = require('./common.js');
const fs = require('fs');
const path = require('path');
const tinify = require("tinify");
tinify.key = "9MqclqXzOqCQB2p9gT7SmlO4i7zu62vQ";
var archiver = require('archiver');
const exiftool = require('node-exiftool');
const exiftoolBin = require('dist-exiftool');
const cTable = require('console.table');
var parser = require('xml2json');
const tmpDirectory = 'tmp/';

try {

  //validate arguments
  common.validateArgumentsForOptimizationAnalysis(process.argv)
  .then((args) => {
    //clear temp directory
    common.clearTmpDirectory(tmpDirectory)
    .then((directory) => {
      //unzip source content package on clear temp directory
      common.unzip(args.sourceZipFile, directory)
      .then((directory) => {
        //read directory and retrieve all files there
        common.readDirectory(directory)
        .then((images) => {
          //filter assets
          common.filterAssets(images, args.size)
            .then((filteredImages) => {
              console.log()
              console.log('Filtered ' + filteredImages.length + ' images above ' + args.size + ' bytes')
              console.table(filteredImages)
            })
            .catch((err5) => {console.log(err5)})
        })
        .catch((err4) => {console.log(err4)})
      })
      .catch((err3) => { console.log(err3) })
    })
    .catch((err2) => console.log(err2))
  })
  .catch((err1)=>console.log(err1))

} catch (e){
  console.log('Catched exception ')
  console.log(e)
}

var rimraf = require("rimraf");
var fs = require('fs');
var AdmZip = require('adm-zip');
const path = require('path');
const { readDirDeep, readDirDeepSync } = require('read-dir-deep');
const exiftool = require('node-exiftool');
const exiftoolBin = require('dist-exiftool');
var exclude = ['application/vnd.adobe.photoshop'];
/**
  Validate the needed arguments for optimization are present
  [0] - Node
  [1] - Script name
  [2] - Source content package
  [3] - Target package name (package name is not the same as file name)

  Reject the promise if invalid arguments were passed in
  Resolve the promise with a true value if all is good
*/
validateArgumentsForOptimizationAnalysis = function(args){
  return new Promise(function(resolve, reject){

    console.log('Validating arguments')
    //validate source content package is present
    if(!args[2]){
      reject('Required "source zip file" argument missing.')
      //throw new Error('Required "source zip" file missing.')
    }

    var result = {sourceZipFile:args[2]};
    if(args[3]){
      result.size = args[3] * 1024;
    } else {
      result.size = 300 * 1024;
    }

    resolve(result);
  });
}

/**
  Validate the needed arguments for optimization are present
  [0] - Node
  [1] - Script name
  [2] - Source content package
  [3] - Target package name (package name is not the same as file name)

  Reject the promise if invalid arguments were passed in
  Resolve the promise with a true value if all is good
*/
validateArgumentsForImageOptimization = function(args){
  return new Promise(function(resolve, reject){

    console.log('Validating arguments for optimization')
    var arguments = {}
    //validate source content package is present
    if(!args[2]){
      reject('Required "source zip file" argument missing.')
      //throw new Error('Required "source zip" file missing.')
    }
    //validate package name is present
    if(!args[3]){
      reject('Required "package name" argument missing')
      //throw new Error('Required "target zip" file missing')
    }

    if(args[4]){
      arguments.size = args[4] * 1024;
    } else {
      arguments.size = 300 * 1024;
    }

    arguments.sourceZipFile=args[2];
    arguments.targetPackageName=args[3];
    resolve(arguments)
  });
}
/**
  Just clear the temp directory given as parameter

  Reject the promise if invalid arguments were passed in
  Resolve the promise with a true value if all is good

*/
function clearTmpDirectory(tmpDirectory){
  return new Promise(function(resolve, reject){
    console.log('Cleaning temp directory...')
    rimraf(tmpDirectory, function () {
      rimraf('HTML_Report', function () {
        console.log("Temp directory clear...");
        resolve(tmpDirectory);
      });
    });
  });
}

/**
  Unzip the package file given as first parameter into the temporal directory
  given as second parameter
*/
function unzip(sourceZipFile, tmpDirectory){
  return new Promise(function(fulfill, reject){
    try {
      console.log('Unzipping ' + sourceZipFile);
      var zip = new AdmZip(sourceZipFile);
      zip.extractAllTo(tmpDirectory, true);
      console.log('Unzipped ' + sourceZipFile);
    } catch (ex){
      reject(ex);
    }
    fulfill(tmpDirectory);
  });
}

function readDirectory(rootDir){
  return new Promise(function(resolve, reject){
    readDirDeep(__dirname + path.sep + rootDir)
      .then((res) => resolve(res))
      .catch((err) => reject(err))
  });
}

function readFile(filename){
  return new Promise(function(fulfill, reject){
    fs.readFile( filename, function(err, data) {
      if(err){
        reject(err);
      } else {
        fulfill(data);
      }
    });
  });
}

function writeFile(filename, xml){
  return new Promise(function(fulfill, reject){
    fs.writeFile(filename, xml, function(err, data) {
      if (err) {
        reject(err);
      }
      else {
        fulfill(filename);
      }
    });
  });
}

var pullMetadata = function(imagePath){
  return new Promise(function(fulfill, reject){
    var ep = new exiftool.ExiftoolProcess(exiftoolBin);
    ep
      .open()
      .then(() => ep.readMetadata(imagePath, ['-File:all']))
      .then((res) => { fulfill( {imagePath, metadata:res} ) })
      .then(() => ep.close())
      .catch(() => reject(imagePath));
  });
}

var isOptimizable = function(imagePath, size){
    return new Promise(function(fulfill, reject){

      stats = fs.lstatSync(imagePath);
      //1- filter by filename & size
      if(stats.isFile() && ( imagePath.includes('.png') || imagePath.includes('.jpg') )
        && !imagePath.endsWith('.xml')
        && stats.size > size ){
          //2 - filter by metadata
          pullMetadata(imagePath)
          .then((res) => {
            var format = res.metadata.data[0].Format;
            fulfill({imagePath, 'isOptimizable':!exclude.includes(format)})
          }).catch((err) => {
            fulfill({imagePath, 'isOptimizable':false})
          });
      } else {
        fulfill({imagePath, 'isOptimizable':false})
      }
    });
}

/**
Just return the size for a given file - Within promise
*/
var giveFileSize = function(path) {
  return new Promise(function(resolve, reject){
    const stats = fs.statSync(path);
    const bytes = stats.size;
    const kb = (bytes/1024).toFixed(2);
    resolve({path, bytes, kb});
  });
}

/**
Just return the size for a given file - Synchronous
*/
var giveFileSizeSync = function(path) {
  const stats = fs.statSync(path);
  return stats.size;
}

function filterAssets(items, limit){
  return new Promise(function(fulfill, reject){
    //var optimizableImages = items.map(isOptimizable);
    var optimizableImages = new Array()
    items.forEach((item) => { optimizableImages.push(isOptimizable( item, limit ))});
    var results = Promise.all(optimizableImages);
    results.then(optimizableAssets => {
      var images = new Array();
      optimizableAssets.forEach((asset) => { if(asset.isOptimizable){ images.push(asset.imagePath) } });
      var results = Promise.all(images.map(giveFileSize));
      results.then((data) => { fulfill(data) });
      }
    )
    .catch((err)=>{console.log(err)});
  });
}

var buildJcrContentNodePath = function(imagePath){
  var patt1=/(.jpg|.png)/;
  m = imagePath.match(patt1);
  var location = imagePath.substring(0, (m.index+4)) + '/.content.xml'
  return location;
}

var getLocationOfSourcePackage = function(sourcePackage){
  return sourcePackage.substr(0, sourcePackage.lastIndexOf('/')+1);
}

var getAssetFilename = function(assetUrl){
  var assetName = buildJcrContentNodePath(assetUrl)
  assetName = assetName.replace('/.content.xml','');
  var lastIndexOf = assetName.lastIndexOf('/')
  assetName = assetName.substring(lastIndexOf+1, assetName.length);
  return assetName;
}

module.exports.validateArgumentsForOptimizationAnalysis = validateArgumentsForOptimizationAnalysis;
module.exports.validateArgumentsForImageOptimization = validateArgumentsForImageOptimization;
module.exports.clearTmpDirectory = clearTmpDirectory;
module.exports.unzip = unzip;
module.exports.readDirectory = readDirectory;
module.exports.readFile=readFile;
module.exports.writeFile=writeFile;
module.exports.filterAssets=filterAssets;
module.exports.pullMetadata=pullMetadata;
module.exports.giveFileSizeSync=giveFileSizeSync;
module.exports.buildJcrContentNodePath=buildJcrContentNodePath;
module.exports.getLocationOfSourcePackage=getLocationOfSourcePackage;
module.exports.getAssetFilename=getAssetFilename;


========

Image Optimization POC
========

**Setup**
--------

`npm install`

**Usage**
--------

To run this application use the nodejs command:
`npm start /Users/framurua/Documents/Genentech/ImageOptimization/Content-Package-from-Prod.zip Optimized-Content-Package [size:300]`

First argument is the content package that contains all images to optimize

Second argument is the package name for the resulting package (Since it is a package name it should not have any extension)

Third argument is optional and is used as a filter to only optimize png or jpg images bigger than this parameter. Default value is 300 (300Kb)

**Report**
--------
The tool will generate an HTML report to easily compare original images against optimized ones.
